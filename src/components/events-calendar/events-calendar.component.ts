import { Component, Input, OnChanges, ViewEncapsulation } from '@angular/core';
import { CalendarEvent, CalendarView } from 'angular-calendar';
import { Subject } from 'rxjs';
import { DateFormatter } from '../../advanced-date.formatter';
import { isSameDay, isSameMonth } from 'date-fns';
import { Router } from '@angular/router';
import { ConfigurationService } from '@universis/common';
import { DatePipe } from '@angular/common';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'universis-events-calendar',
  templateUrl: './events-calendar.component.html',
  styles: [
    `.cal-month-view .cal-open-day-events {
      padding: 15px;
      color: #000;
      background-color: #f3f3f3;
      box-shadow: inset 0 0 10px 0 rgb(137 128 128 / 50%)
    }`,
    `.cal-week-view .cal-event, .cal-week-view {
      overflow: hidden;
      text-overflow: hidden;
      white-space: normal;
    }`,
    `.cal-week-view .cal-time-events .cal-event {
      line-height: 20px;
    }`
  ],
  encapsulation: ViewEncapsulation.None
})
export class EventsCalendarComponent implements OnChanges {

  @Input() events: any;
  @Input() commandsToEvent: any [];
  @Input() isAllowed = true;

  public CalendarView = CalendarView;
  public view: CalendarView = CalendarView.Week;
  public locale: string;
  public newEventSubject: Subject<any> = new Subject();
  public viewDate: Date = new Date();
  public activeDayIsOpen = false;

  constructor(
    private router: Router,
    private _configService: ConfigurationService,
    private datePipe: DatePipe,
    private _translateService: TranslateService) {
    this.locale = this._configService.currentLocale;
  }

  ngOnChanges(): void {
    if (this.isAllowed && this.commandsToEvent) {
      this.events = this.events.map(x => {
        return {
          id: x.id,
          title: x.name,
          start: x.startDate,
          end: x.endDate,
          performer: x.performer,
          location: x.location,
          additionalType: x.additionalType,
          courseClass: x.courseClass,
          allDay: false,
          url: x.url,
          color: {
            primary: DateFormatter.stringToColor(x.name),
            secondary: DateFormatter.LightenDarkenColor(DateFormatter.stringToColor(x.name), 140),
            secondaryText: '#000000'
          },
          actions: [
            {
              label: '<i class="fas fa-eye"></i>',
              a11yLabel: 'Preview',
              onClick: ({ event }: { event: CalendarEvent }): void => {
                this.navigateToEvent(event);
              },
            },
          ]
        };
      });
    } else {
      this.events = this.events.map(x => {
        return {
          id: x.id,
          title: x.name,
          start: x.startDate,
          end: x.endDate,
          performer: x.performer,
          location: x.location,
          additionalType: x.additionalType,
          courseClass: x.courseClass,
          allDay: false,
          url: x.url,
          color: {
            primary: DateFormatter.stringToColor(x.name),
            secondary: DateFormatter.LightenDarkenColor(DateFormatter.stringToColor(x.name), 140),
            secondaryText: '#000000'
          },
        };
      });
    }
    this.newEventSubject.next(undefined);
  }

  public navigateToEvent(event) {
    if (event.url) {
      this.router.navigate([event.url]);
    }
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      this.activeDayIsOpen = !((isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) || events.length === 0);
      this.viewDate = date;
    }
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  generateEventTitle(event: any): string {
    try {
      const locationDescription = event.location.name ? `<div style="margin-bottom:3px">${event.location.name}</div>` : '';
      const performerAlternate = event.performer.alternateName ? `<div>${event.performer.alternateName}</div>` : '';
      // tslint:disable-next-line:max-line-length
      const eventType = event.additionalType ? `<div>${this._translateService.instant('Events.EventTypes.' + event.additionalType)}</div>` : '';
      const timeStart = this.datePipe.transform(event.start, 'HH:mm');
      const time = `<div><b>${timeStart}</b></div>`;
      return `${time}<div><b>${event.title}</b></div>${eventType}${locationDescription}${performerAlternate}`;
    } catch (error) {
      console.error(error);
    }
  }
}
