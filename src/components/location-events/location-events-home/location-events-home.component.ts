import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ReferrerRouteParams, ReferrerRouteService } from '@universis/common';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { AngularDataContext } from '@themost/angular';

declare interface TablePathConfiguration {
  name: string;
  alternateName: string;
  show?: boolean;
}

@Component({
  selector: 'universis-location-events-home',
  templateUrl: './location-events-home.component.html'
})
export class LocationEventsHomeComponent implements OnInit, OnDestroy {
  private _referrerSubscription: Subscription;
  private _paramSubscription: Subscription;
  private _listType: string;
  public recordsTotal: number;
  public referrerRoute: ReferrerRouteParams = {
    commands: ['/events/list/open']
  };
  public paths: Array<TablePathConfiguration> = [];
  public activePaths: Array<TablePathConfiguration> = [];
  public location: {
    id: number,
    name: string
  };

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _httpClient: HttpClient,
    private _context: AngularDataContext,
    public referrer: ReferrerRouteService
  ) {}

  ngOnInit(): void {
    this._httpClient.get('assets/lists/LocationEvents/upcoming.all.config.json').subscribe((config: any) => {
      this.paths = config.paths;
      this.activePaths = this.paths.filter(x => {
        return x.show === true;
      }).slice(0);
      this._paramSubscription = this._activatedRoute.firstChild.params.subscribe(async params => {
        this._listType = params.list;
        try {
          this.location = await this._context
            .model(`Places/${params.id}`)
            .select('id, name')
            .getItem();
        } catch (err) {
          console.error(err);
        }
      });
    });

    this._referrerSubscription = this.referrer.routeParams$.subscribe(result => {
      this.referrerRoute = result || {
        commands: ['/events/list/open']
      };
    });
  }

  /**
   * In the LocationEventsTableComponent the url changes when clicking the table actions but the tab should stay active.
   * For example, the tab 'All' defaults to the path 'list/all', but it should stay active for the paths 'list/recursive' and
   * 'list/individual' as well.
   */
  isActive(itemPath): boolean {
    if (itemPath.split('/')[0] === this._listType) {
      return true;
    }
   return false;
  }

  ngOnDestroy(): void {
    if (this._referrerSubscription) {
      this._referrerSubscription.unsubscribe();
    }
    if (this._paramSubscription) {
      this._paramSubscription.unsubscribe();
    }
  }
}
